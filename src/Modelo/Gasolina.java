/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author felip
 */
public class Gasolina {
    private String idGasolina;
    private String tipo;
    private float precio;
    
    public Gasolina(){
        
    }
    
    public Gasolina(Gasolina GAS){
        this.idGasolina = GAS.idGasolina;
        this.precio = GAS.precio;
        this.tipo = GAS.tipo;
    }
    
    public Gasolina(String idGasolina, String tipo, float precio){
        this.idGasolina = idGasolina;
        this.precio = precio;
        this.tipo = tipo;
    }
    
    public void setId(String idGasolina){
        this.idGasolina = idGasolina;
    }
    
    public void setTipo(String tipo){
        this.tipo = tipo;
    }
    
    public void setPrecio(float precio){
        this.precio = precio;
    }
    
    public String getId(){
        return this.idGasolina;
    }
    
    public String getTipo(){
        return this.tipo;
    }
    
    public float getPrecio(){
        return this.precio;
    }
    
    
    
}
