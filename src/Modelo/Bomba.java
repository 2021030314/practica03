/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author felip
 */
public class Bomba {
    private int numBomba;
    private float capacidad;
    private float acumulador;
    private Gasolina gasolina;
    
    public Bomba(){
        
    }
    
    public Bomba(Bomba BOMB){
        this.acumulador = BOMB.acumulador;
        this.capacidad = BOMB.capacidad;
        this.numBomba = BOMB.numBomba;
        this.gasolina = BOMB.gasolina;
    }
    
    public Bomba(int numBomba, float capacidad, float acumulador, Gasolina gasolina){
        this.acumulador = acumulador;
        this.capacidad = capacidad;
        this.numBomba = numBomba;
        this.gasolina = gasolina;
    }
    
    public void setBomba(int numBomba){
        this.numBomba = numBomba;
    }
    
    public void setCapacidad(float capacidad){
        this.capacidad = capacidad;
    }
    
    public void setAcumulador(float acumulador){
        this.acumulador = acumulador;
    }
    
    
    public void setGas(Gasolina gasolina){
        this.gasolina = gasolina;
    }
    
    public int getBomba(){
        return this.numBomba;
    }
    
    public float getCapacidad(){
        return this.capacidad;
    }
    
    public float getAcumulador(){
        return this.acumulador;
    }
    
    public Gasolina getGas(){
        return this.gasolina;
    }
    
    public float inventarioGas(){
        return this.capacidad - this.acumulador;
    }
    
    public float ventaTotal(){
        return this.acumulador * this.gasolina.getPrecio();
    }
    
    public float venderGas(float cantidad){
        this.acumulador += cantidad;
        return cantidad * this.gasolina.getPrecio();
    }
    
    public void iniciarBomba(int idBomba, Gasolina gasolina){
        this.numBomba = idBomba;
        this.acumulador = 0;
        this.capacidad = 200;
        this.gasolina = gasolina; 
    }
}
