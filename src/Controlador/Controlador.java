/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;
import Modelo.Bomba;
import Modelo.Gasolina;
import Vista.dlgBomba;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
/**
 *
 * @author felip
 */
public class Controlador implements ActionListener {
    private Bomba BOMB;
    private Gasolina GAS;
    private dlgBomba vista;
    
    private Controlador(Bomba BOMB, dlgBomba vista, Gasolina GAS){
        this.BOMB = BOMB;
        this.vista = vista;
        this.GAS = GAS;
        
        vista.btnIniciar.addActionListener(this);
        vista.btnRegistrar.addActionListener(this);
    }
    
     private void iniciarV(){
        vista.setTitle("-¡- BOMBA DE GASOLINA -¡-");
        vista.setSize(805, 605);
        vista.setVisible(true);
    }
    
    private void enableText(){
        vista.txtNumBomba.enable(true);
        vista.txtCantidad.enable(true);
    }
    
    private void cleanText(){
        vista.labelCosto.setText(null);
        vista.labelInv.setText(null);
        vista.labelLitros.setText(null);
        vista.labelVentas.setText(null);
        vista.txtCantidad.setText(null);
    }
    
    public static void main(String[] args) {
       Bomba BOMB = new Bomba();
       Gasolina GAS = new Gasolina();
       dlgBomba vista = new dlgBomba(new JFrame(), true);
       Controlador CRT = new Controlador(BOMB, vista, GAS);
       CRT.iniciarV();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == vista.btnIniciar){
            GAS.setPrecio(Float.parseFloat(vista.txtPrecioV.getText()));
            GAS.setTipo(vista.comboGas.getSelectedItem().toString());
            GAS.setId(String.valueOf(vista.comboGas.getSelectedIndex())); 
            try{
                BOMB.iniciarBomba(Integer.parseInt(vista.txtNumBomba.getText()), GAS);
                vista.sliderBomba.setMaximum(200);
                cleanText();
                enableText();
                
                vista.btnRegistrar.setEnabled(true);
                vista.txtContador.setText(String.valueOf(0));
                vista.sliderBomba.setValue((int)BOMB.getCapacidad());    
            }
            catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: " + ex.getMessage());
            }
        }
        
        if(e.getSource() == vista.btnRegistrar){
            int contador = Integer.parseInt(vista.txtContador.getText());
            try{
                float cantidad = Float.parseFloat(vista.txtCantidad.getText());
                if(cantidad <= BOMB.inventarioGas()){
                    vista.labelCosto.setText(String.valueOf(BOMB.venderGas(cantidad)));
                    vista.labelLitros.setText(String.valueOf(BOMB.getAcumulador()));
                    vista.labelInv.setText(String.valueOf(BOMB.inventarioGas()));
                    vista.labelVentas.setText(String.valueOf(BOMB.ventaTotal()));
                    contador ++;
                    vista.txtContador.setText(String.valueOf(contador));
                    vista.sliderBomba.setValue((int)BOMB.inventarioGas());
                }
                else {
                    JOptionPane.showMessageDialog(vista, "Cantidad excedida de los límites");
                }
            }
            catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: " + ex.getMessage());
            }
            
        }
    }
}
